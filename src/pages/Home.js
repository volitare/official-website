import React, {useState} from 'react';
import { motion } from 'framer-motion';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Box, Typography, Button } from '@mui/material';
// import bannerTitle from '../assets/example_banner_title.png';
import NewsletterForm from '../components/NewsletterForm';
import bannerGif from '../assets/GameVoltBanner.png';
// import asteroid from '../assets/asteroid.gif';
// import chess from '../assets/chess.gif';
// import rts from '../assets/rts.gif';
// import shmup from '../assets/shmup.gif';
// import space from '../assets/space.gif';
// import turn_based from '../assets/turn-based-rpg.gif';
// import lock from '../assets/lock.png';
import chess from '../assets/Chess Cover.png';
import checkers from '../assets/Checkers Cover.png';
import fourInARow from '../assets/4-in-a-row Cover.png';
import minesweeper from '../assets/Minesweeper Cover.png';
import codebreakers from '../assets/Codebreakers Cover.png';
import gemini from '../assets/Gemini Cover.png';
import './styles/carousel.css';
import './styles/gameImage.css';
import backgroundImage from '../assets/nebula4k.jpg';

const BannerEntry = ({ gameName, promoText, buttonText, imgSrc }) => (
  <div style={{ position: 'relative' }}>
    <img src={imgSrc} alt={gameName} style={{ width: '100%' }} />
    <div 
      style={{ 
        position: 'absolute', 
        top: 0, 
        left: 0, 
        height: '100%', 
        width: '40%', 
        backgroundColor: 'rgba(0, 0, 0, 0)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          position: 'relative',
          width: '80%', // adjust as needed
          height: '84%', // adjust as needed
          color: 'white',
          backgroundColor: 'rgba(0, 0, 0, 0)',
          padding: '10px',
          borderRadius: '25px',
        }}
      >
        <Typography variant="h3" sx={{textShadow:'2px 2px 8px rgba(0, 0, 0, 0.9)', fontFamily: 'Baskic8', fontWeight: 'bold' }}>{gameName}</Typography>
        <Typography variant="h5" sx={{textShadow:'2px 2px 8px rgba(0, 0, 0, 0.9)', fontFamily: 'Baskic8', fontWeight: 'bold' }}>{promoText}</Typography>
        <Button variant="contained" color="primary" sx={{ 
            mt: 2,
            fontSize: '1.25rem', // Bigger text
            paddingLeft: '30px',
            paddingRight: '30px',
            paddingTop: '20px',
            borderRadius: '20px', // More rounded corners
            fontFamily: 'Baskic8', // Custom font
            fontWeight: 'bold',
            backgroundColor: '#CB905A',
            textAlign: 'center',
         }}>{buttonText}</Button>
      </Box>
    </div>
  </div>
);

const GameImage = ({ src, alt, buttonText = 'Discover', overlayText = 'Lorem ipsum dolor sit amet' }) => {
  const [hover, setHover] = useState(false);

  return (
    <div 
      className="game-image-container" 
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <img src={src} alt={alt} className="game-image"/>
      <div className={`overlay ${hover ? 'hover' : ''}`}>
        <h1 className="game-title">{alt}</h1>
        <p className="overlay-text">{overlayText}</p>
        <Button variant="contained" color="primary" sx={{
          marginTop: '1em', /* Base margin */
          fontSize: '1vw', /* Use viewport width units for responsive font size */
          padding: '0.5em 1em', /* Base padding */
          borderRadius: '20px',
          fontFamily: 'Baskic8',
          fontWeight: 'bold',
          backgroundColor: '#CB905A',
          textAlign: 'center',
        }}>
          {buttonText}
        </Button>
      </div>
    </div>
  );
};

const Home = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{
        display: 'flex',
        flexDirection: 'column',
        // justifyContent: 'space-between',
        minHeight: '100vh', // This makes sure the content takes up at least the full height of the viewport
        alignItems: 'center',
        // color: 'white',
        overflowX: 'hidden',
      }}
    >
      {/* <h1>Home Page</h1>
      <p>Welcome to Indie Studio!</p> */}
      <Carousel
        showArrows={true}
        showThumbs={false}
        infiniteLoop={true}
        autoPlay={true}
        interval={5000}
        dynamicHeight={false}
      >
        <BannerEntry gameName="GameVolt" promoText="Collect and play a wide range of cartridges in a virtual console!" buttonText="Discover" imgSrc={bannerGif} />
        {/* <BannerEntry gameName="Game 2" promoText="Insert promotional text here!" buttonText="Play Game 2" imgSrc={bannerGif} /> */}
      </Carousel>
      <Box sx={{
        backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${backgroundImage})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        padding: '30px 0',
      }}>
        <Typography variant="h4" align="center" gutterBottom sx={{color: "white", fontFamily: 'Baskic8', fontWeight: 'bold' }}>
            Our Games
        </Typography>
        <Carousel
          showArrows={true}
          showThumbs={false}
          infiniteLoop={true}
          autoPlay={true}
          interval={5000}
          dynamicHeight={false}
          centerMode={true}
          centerSlidePercentage={20} // Adjust this percentage to fit the number of items per slide
          emulateTouch={true}
        >
          <GameImage src={checkers} alt="Checkers" overlayText="Checkers, also known as draughts, is a classic board game of tactical skill and strategy, where players aim to capture or block all of the opponent's pieces." />
          <GameImage src={chess} alt="Chess" overlayText="Chess is a strategic board game that pits two players against each other in a battle of wits, with the objective of checkmating the opponent's king while protecting one's own." />
          <GameImage src={codebreakers} alt="Codebreakers" overlayText='Codebreakers is a challenging puzzle game where players attempt to decipher a hidden code or sequence through logic and deduction within a limited number of tries.' />
          <GameImage src={fourInARow} alt="Four-in-a-Row" overlayText="Four-in-a-Row is a two-player connection game in which the players first choose a color and then take turns dropping colored discs into a grid, with the objective being to connect four of one's own discs of the same color next to each other vertically, horizontally, or diagonally before the opponent." />
          <GameImage src={gemini} alt="Gemini" overlayText='Gemini is a match-three puzzle game where players swap adjacent gems to form chains of three or more of the same color, aiming for high scores through combo chains and speed.' />
          <GameImage src={minesweeper} alt="Minesweeper" overlayText='Minesweeper is a single-player puzzle game where the challenge is to clear a minefield without detonating any mines, using clues about the number of neighboring mines in each field.' />
        </Carousel>
      </Box>
      <NewsletterForm />
    </motion.div>
  );
};

export default Home;
