---
title: "GameVolt: First Look"
date: "2025-03-10"
author: "Andrew Park"
image: "/assets/GameVolt Release Banner.png"
slug: "gamevolt-first-look"
---

Today, we are thrilled to announce the launch of our new game! We have been working hard to setup a cohesive platform and are already at work creating new cartridges and updates post launch.

GameVolt is a virtual console where you can collect and play a variety of original game cartridges, whether on your own or with your friends!

**GameVolt is not an emulator and only supports games created by Volitare Studios for this console.**

<!-- more -->

## FEATURES

### Cartridges

• Compact games spanning a wide range of genres.

• You start with 6 cartridges and can earn coins to unlock new cartridges.

• New cartridges or updates to existing ones will be continuously added.

### Multiplayer

• Play games with your friends on supported cartridges.

• You must have an account to enable this feature.

### Account Sync

• Create an account to unlock cloud saves, multiplayer, and additional offerings!

• Sync your settings and collection to access them on any device.

### Collection
• Earn achievements and cosmetics you can show off on your profile as you collect and play through our cartridges!

### Controller Support
• Built-in Touch Controller with optional haptics.
• Connect physical controllers to your device via bluetooth if you prefer a more traditional experience.

Check out the details below and join us on this exciting journey.
