# Privacy Policy

## Introduction

**Volitare Studios** ("we", "our", or "us") is committed to protecting your privacy. This Privacy Policy explains how we collect, use, disclose, and safeguard your information when you visit our website, [Volitare Studios](https://www.volitarestudios.com), including any other media form, media channel, mobile website, or mobile application related or connected thereto (collectively, the "Site"). Please read this privacy policy carefully. If you do not agree with the terms of this privacy policy, please do not access the site.

## Information We Collect

We may collect information about you in a variety of ways. The information we may collect on the Site includes:

### Personal Data

Personally identifiable information, such as your name, shipping address, email address, and telephone number, and demographic information, such as your age, gender, hometown, and interests, that you voluntarily give to us when you register with the Site or when you choose to participate in various activities related to the Site, such as online chat and message boards.

### Derivative Data

Information our servers automatically collect when you access the Site, such as your IP address, your browser type, your operating system, your access times, and the pages you have viewed directly before and after accessing the Site.

### Financial Data

Financial information, such as data related to your payment method (e.g., valid credit card number, card brand, expiration date) that we may collect when you purchase, order, return, exchange, or request information about our services from the Site. We store only very limited, if any, financial information that we collect. Otherwise, all financial information is stored by our payment processor, and you are encouraged to review their privacy policy and contact them directly for responses to your questions.

## Use of Your Information

Having accurate information about you permits us to provide you with a smooth, efficient, and customized experience. Specifically, we may use information collected about you via the Site to:

- Create and manage your account.
- Process your transactions.
- Email you regarding your account or orders.
- Fulfill and manage purchases, orders, payments, and other transactions related to the Site.
- Generate a personal profile about you to make future visits to the Site more personalized.
- Increase the efficiency and operation of the Site.
- Monitor and analyze usage and trends to improve your experience with the Site.
- Notify you of updates to the Site.
- Request feedback and contact you about your use of the Site.
- Resolve disputes and troubleshoot problems.
- Respond to product and customer service requests.
- Send you a newsletter.

## Disclosure of Your Information

We do not share, sell, rent or trade your information with third parties for their promotional purposes.

## Security of Your Information

We use administrative, technical, and physical security measures to help protect your personal information. While we have taken reasonable steps to secure the personal information you provide to us, please be aware that despite our efforts, no security measures are perfect or impenetrable, and no method of data transmission can be guaranteed against any interception or other types of misuse.

## Policy for Children

We do not knowingly solicit information from or market to children under the age of 13. If we learn that we have collected personal information from a child under age 13 without verification of parental consent, we will delete that information as quickly as possible. If you believe we might have any information from or about a child under 13, please contact us at [support@volitarestudios.net](mailto:support@volitarestudios.net).

## Changes to This Privacy Policy

We may update this Privacy Policy from time to time in order to reflect, for example, changes to our practices or for other operational, legal, or regulatory reasons. Please review this Privacy Policy periodically for any changes. Your continued use of the Site after the posting of any changes to this policy means you accept those changes.

## Contact Us

If you have questions or comments about this Privacy Policy, please contact us at:

Volitare Studios  
[support@volitarestudios.net](mailto:support@volitarestudios.net)  
